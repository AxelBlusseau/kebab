const cliSelect = require('cli-select');
const chalk = require('chalk');
const Methods = require('./functions')
const methods = new Methods()

const Constants = require('./consts')
const consts = new Constants()


function main() {
    methods.displayIngredients()
    let ingredients = methods.getIngredients();
    methods.isVegan(ingredients) 
    methods.askDoubleCheese(ingredients)
    methods.displayMyIngredients(ingredients)
}



main();