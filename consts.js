var Constants = function() {
    this.ingredients = [
            {nom: "Pain", vege: true, pesci: true, valeur: 2, qty: 1, type: "Pain"},
            
            //Viandes
            {nom: "Doner", vege: false, pesci: false, valeur: 2, qty: 1, type: "Viande"},
            {nom: "Poisson", vege: false, pesci: true, valeur: 2, qty: 1, type: "Viande"},
            {nom: "Crevette", vege: false, pesci: true, valeur: 2, qty: 1, type: "Viande"},
            {nom: "Surimi", vege: false, pesci: true, valeur: 2, qty: 1, type: "Viande"},
            {nom: "Poulet", vege: false, pesci: false, valeur: 2, qty: 1, type: "Viande"},
            
            //Sauce
            {nom: "Ketchup", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            {nom: "Mayo", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            {nom: "Moutarde", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            {nom: "BBQ", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            {nom: "Samurai", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            {nom: "Blanche", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            {nom: "Bechamel", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            {nom: "Cheddar", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            {nom: "Algerienne", vege: true, pesci: true, valeur: 2, qty: 1, type: "Sauce"},
            
            //Légumes
            {nom: "Salade", vege: true, pesci: true, valeur: 2, qty: 1, type: "Legume"},
            {nom: "Oignons", vege: true, pesci: true, valeur: 2, qty: 1, type: "Legume"},
            {nom: "Tomate", vege: true, pesci: true, valeur: 2, qty: 1, type: "Legume"},

            //Fromages
            {nom: "Cheddar", vege: true, pesci: true, valeur: 2, qty: 1, type: "Fromage"},
            {nom: "Raclette", vege: true, pesci: true, valeur: 2, qty: 1, type: "Fromage"},
            {nom: "Maroille", vege: true, pesci: true, valeur: 2, qty: 1, type: "Fromage"}
    ]
}
module.exports = Constants