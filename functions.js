const Constants = require('./consts')
const consts = new Constants()
const prompt = require('prompt-sync')();

var Methods = function() {


    /* ISVEGAN */

    this.isVegan = function(ingredients){ // Fonction qui va verifier si une liste d'ingredient est vege
        let isVegan = true;
        ingredients.forEach(ing => { // on itere sur chaque ingredient
            if(!ing.vege) isVegan = false; // si il ne l'est pas alors le sandwich ne le sera pas
        })
        if(isVegan) {
            console.log("Ce Kebab est certifié 100% Vegan")
        }else{
            console.log("Attention, ce Kebab n'est pas Vegan")
            this.isPescitarian(ingredients)
        }
    }
    
    /* ISPESCITARIAN */

    this.isPescitarian = function(ingredients){
        let isPesci = true;
        ingredients.forEach(ing => { // on itere sur chaque ingredient
            if(!ing.pesci) isPesci = false; // si il ne l'est pas alors le sandwich ne le sera pas
        })
        if(isPesci) {
            console.log("Ce Kebab est certifié 100% Pescitarien")
        }else{
            console.log("Attention, ce Kebab n'est pas Pescitarien")
        }
    }

    /* GETINGREDIENTS */

    this.getIngredients = function(){ // Focntion qui va demander a l'user de saisir ses ingredients
    let ingredients = [consts.ingredients[0]]; // On met le pain automatiquement car il est present dans chaque kebab
    let input;
    let inputG;
    let aliment;
    
    while(inputG != "Non") {
        input = " ";
        inputG = " ";

        while(input != "") {
            input = prompt("Quels ingrédients voulez vous dans votre Kebab ? : ");
            if (consts.ingredients.find((ing) => ing.nom == input)) {
                aliment = consts.ingredients.find((ing) => ing.nom == input);
                // Si l'aliment est déjà dans le tableau, incrémente la quantité, sinon rajoute l'aliment
                ingredients.find((ing) => ing.nom == aliment.nom) ? ingredients.find((ing) => ing.nom == aliment.nom).qty +=1 : ingredients.push(aliment);
                input = "";
            }
            else console.log("Veuillez choisir un ingrédient valide");
        }

        while(inputG != "Non" && inputG != "Oui") {
            inputG = prompt("Souhaitez-vous rajouter un ingrédient ? (Oui/Non)");
        }
    }
    
    return ingredients;
}

/* DISPLAY INGREDIENT */

this.displayIngredients = function() { // fonction qui va afficher les ingredients au debut de l'appli
    console.log("### LISTE DES INGREDIENTS ###");
    let category = "";
    consts.ingredients.forEach(ing => {
        if (ing.type != category) {
            category = ing.type;
            console.log(`------- ${category} -------`);
        }
            
        console.log(ing.nom);
    })
    console.log("#############################");
}

/* ASK DOUBLE CHEESE */

this.askDoubleCheese = function(ingredients){
    let input = " ";
    while(input != "") {
        input = prompt("Voulez vous doubler la quantité de Fromage ? (Oui/Non)");
        if (input === 'Oui') {
            this.doubleCheese(ingredients);
            input = "";
        }else{
            input = "";
        }
    }
}

/* DOUBLE LE FROMAGE */

this.doubleCheese = function(ingredients) { // va doubler tout les fromages du sandwich
    ingredients.forEach((ing) => {
        if (ing.type == "Fromage") ing.qty = ing.qty * 2
    });
    return ingredients;
}


this.displayMyIngredients = function(ingredients) {
    console.log("Kebab composé de : ");
    ingredients.forEach(element => {
        console.log("Ingredient : " + element.nom + ", Quantite : " + element.qty);
    });
}
    
}

module.exports = Methods